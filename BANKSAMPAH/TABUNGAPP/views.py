from django.shortcuts import render
from . import models

# Create your views here.
def home(request):
    daftar_jenis_sampah = models.NabungSampah.objects.all()
    validasi_sampah = models.Setor.objects.all()
    list_validasi = models.ListPage.objects.all()

    context = {
        'daftar_jenis_sampah': daftar_jenis_sampah,
        'sampah_1': daftar_jenis_sampah[0],
        'validasi_sampah': validasi_sampah,
    }

    return render(request, context)
