# Generated by Django 3.2.7 on 2021-10-26 04:35

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='NabungSampah',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('jenis', models.CharField(max_length=65)),
                ('gambar', models.ImageField(upload_to='images')),
                ('harga', models.IntegerField(default=0)),
                ('jumlah', models.IntegerField(default=0)),
                ('total', models.IntegerField(default=0)),
                ('merek', models.CharField(choices=[('Kertas', 'Kardus-Bagus'), ('Kertas', 'Kardus-Jelek'), ('Botol', 'Botol-Kaca'), ('Botol', 'Botol-Plastik'), ('Botol', 'Botol-Kaleng'), ('Sampah', 'Organik'), ('Sampah', 'Non-Organik'), ('Sampah', 'Residu'), ('Sampah', 'Kimia'), ('Sampah', 'Kain')], max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Setor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('validasi', models.CharField(max_length=40)),
                ('total', models.IntegerField(default=0)),
                ('alamat_user', models.CharField(max_length=50)),
                ('id_pesanan', models.IntegerField(default=0)),
            ],
        ),
    ]
