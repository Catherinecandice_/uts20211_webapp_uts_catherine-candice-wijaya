from django.apps import AppConfig


class TabungappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'TABUNGAPP'
