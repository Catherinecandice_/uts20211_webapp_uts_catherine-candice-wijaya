from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.NabungSampah)
admin.site.register(models.Setor)
admin.site.register(models.ListPage)
