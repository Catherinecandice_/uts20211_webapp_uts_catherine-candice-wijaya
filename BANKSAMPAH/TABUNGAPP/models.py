from django.db import models

# Create your models here.
class NabungSampah(models.Model):
    jenis = models.CharField(max_length=65)
    gambar = models.ImageField(upload_to="images")
    harga = models.IntegerField(default=0)
    jumlah = models.IntegerField(default=0)
    total = models.IntegerField(default=0)
    pilihan_jenis = [
        ('Kertas', 'Kardus-Bagus'),
        ('Kertas', 'Kardus-Jelek'),
        ('Botol', 'Botol-Kaca'),
        ('Botol', 'Botol-Plastik'),
        ('Botol', 'Botol-Kaleng'),
        ('Sampah', 'Organik'),
        ('Sampah', 'Non-Organik'),
        ('Sampah', 'Residu'),
        ('Sampah', 'Kimia'),
        ('Sampah', 'Kain'),
    ]
    merek = models.CharField(
        max_length=30,
        choices=pilihan_jenis,
    )

    def __str__(self):
        return self.jenis

class Setor(models.Model):
    validasi = models.CharField(max_length = 40)
    total = models.IntegerField(default=0)
    alamat_user = models.CharField(max_length=50)
    id_pesanan = models.IntegerField(default=0)

    def __str__(self):
        return self.validasi

class ListPage(models.Model):
    tanggal_dan_waktu_setor = models.CharField(max_length = 35)
    penyetor = Setor.validasi()
    jenis_sampah = sum(NabungSampah.jenis())
    jumlah_setor = sum(NabungSampah.harga())
    total_poin = Setor.total()
    status = models.BooleanField(default=True)